#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>

char *ext;
char *p;
int status;

void make_dir(char filename[])
{

    // Get File Name
    char name[100];
    strcpy(name, filename);

    // Get Extension
    ext = strchr(name, '.');

    // Creating Folder Name
    char temp[100];
    strcpy(temp, "./hartakarun/");
    strcat(temp, ext);
    printf("\n%s", temp);

    // Create folder
    status = mkdir(temp, S_IRWXU);
}

int main()
{
    DIR *dir = opendir("./hartakarun");
    if (dir == NULL)
    {
        return 1;
    }

    struct dirent *entity;
    entity = readdir(dir);
    while (entity != NULL)
    {
        char temp[100];
        strcpy(temp, entity->d_name);
        make_dir(temp);

        entity = readdir(dir);
    }

    closedir(dir);

    return 0;
}
