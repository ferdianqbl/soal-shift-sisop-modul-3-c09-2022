# Kelompok C09

- 5025201165 - Gabriel Solomon Sitanggang
- 5025201102 - Arya Nur Razzaq
- 5025201020 - Muhammad Ferdian Iqbal
  <br><br>

# Soal 1

## Persoalan

- Download dua file zip dan unzip file zip tersebut di dua folder yang berbeda dengan nama quote untuk file zip quote.zip dan music untuk file zip music.zip. Unzip ini dilakukan dengan bersamaan menggunakan thread.
- Decode semua file .txt yang ada dengan base 64 dan masukkan hasilnya dalam satu file .txt yang baru untuk masing-masing folder (Hasilnya nanti ada dua file .txt) pada saat yang sama dengan menggunakan thread dan dengan nama quote.txt dan music.txt. Masing-masing kalimat dipisahkan dengan newline/enter.
- Pindahkan kedua file .txt yang berisi hasil decoding ke folder yang baru bernama hasil.
- Folder hasil di-zip menjadi file hasil.zip dengan password 'mihinomenest[Nama user]'. (contoh password : mihinomenestnovak)
- Karena ada yang kurang, kalian diminta untuk unzip file hasil.zip dan buat file no.txt dengan tulisan 'No' pada saat yang bersamaan, lalu zip kedua file hasil dan file no.txt menjadi hasil.zip, dengan password yang sama seperti sebelumnya.

## Penyelesaian

```C++
pthread_t unzipId[2];
```

> Variable tersebut digunakan untuk menampung hasil dari proses thread. Dibuat array dua index karena thread yang dilakukan sebanyak dua proses.

```c++
void *downloadFile(char *down_arg[])
{
  pid_t id = fork();
  int status;
  if (id == 0)
  {
    execv("/bin/wget", down_arg);
  }
  else
  {
    while ((wait(&status)) > 0)
      ;
    printf("download file success\n");
  }
}
void *unzipFile(char *unzip_arg[])
{
  pid_t id = fork();
  int status;
  if (id == 0)
  {
    execv("/bin/unzip", unzip_arg);
  }
  else
  {
    while ((wait(&status)) > 0)
      ;
    printf("unzip file success\n");
  }
}
void rmFile(char *rm_arg[])
{
  pid_t id = fork();
  int status;
  if (id == 0)
  {
    execv("/bin/rm", rm_arg);
  }
  else
  {
    while ((wait(&status)) > 0)
      ;
    printf("remove file success\n");
  }
}
void base64(char *base_arg[])
{
  pid_t id = fork();
  int status;
  if (id == 0)
  {
    execv("/bin/base64", base_arg);
  }
  else
  {
    while ((wait(&status)) > 0)
      ;
    printf("base64 success\n");
  }
}

```

> Beberapa fungsi di atas digunakan untuk mendownload file, unzip file, remove file, dan melakukan base64. Fungsi tersebut juga menggunakan fork.

```C++
void *unzip(void *arg)
{
  char *unz_args[] = {"unzip", "-q", "music.zip", "-d", "music", NULL};
  char *unz_args2[] = {"unzip", "-q", "quote.zip", "-d", "quote", NULL};

  pthread_t id = pthread_self();
  if (pthread_equal(id, unzipId[0]))
  {
    unzipFile(unz_args);
  }
  else if (pthread_equal(id, unzipId[1]))
  {
    unzipFile(unz_args2);
  }
  return NULL;
}

```

> Beberapa fungsi unzip merupakan fungsi yang dijalankan ketika thread dilakukan. Ketika thread dilakukan, fungsi ini akan menjalankan fungsi unzip file sesuai dengan file mana yang akan di unzip. Selain unzip, program ini juga langsung membuat folder sesuai dengan nama file unzipnya.

### POIN A

```C++
  // download file
  char *down_args[] = {"wget", "-q", "https://docs.google.com/uc?export=download&id=1_djk0z-cx8bgISFsMiUaXzty2cT4VZp1", "-O", "music.zip", NULL};
  char *down_args2[] = {"wget", "-q", "https://docs.google.com/uc?export=download&id=1jR67_JAyozZPs2oYtYqEZxeBLrs-k3dt", "-O", "quote.zip", NULL};
  downloadFile(down_args);
  downloadFile(down_args2);
```

> Proses di atas digunakan untuk mendownload file zip dari internet.

```C++
 // unzip file
  pthread_create(&unzipId[0], NULL, &unzip, NULL);
  pthread_create(&unzipId[1], NULL, &unzip, NULL);
  pthread_join(unzipId[0], NULL);
  pthread_join(unzipId[1], NULL);
```

> Setelah file selesai didownload, file zip diunzip menggunakan thread agar dilakukan bersamaan.

```C++
// remove zip
  char *rm_args[] = {"rm", "music.zip", NULL};
  char *rm_args2[] = {"rm", "quote.zip", NULL};
  rmFile(rm_args);
  rmFile(rm_args2);
```

> Setelah berhasil diunzip, file zip yang sudah tidak digunakan dihapus.

```C++
 // base64
  char *base_args[] = {"base64", "-d", "music/m1.txt", NULL};
  base64(base_args);
```

> Baris program ini digunakan untuk mencoba mendecode isi text.

### POIN B, C, D, E

> Pada poin B, kami belum menemukan solusi pengerjaannya. Kami belum menemukan cara memasukkan hasil Decode ke dalam text sehingga pekerjaan tidak bisa kami lanjutkan untuk poin - poin setelahnya.

# Soal 2

## Persoalan

- Pada saat client terhubung ke server, terdapat dua pilihan pertama yaitu register dan login. Jika memilih register, client akan diminta input id dan passwordnya untuk dikirimkan ke server. Data input akan disimpan ke file users.txt dengan format username:password. Jika client memilih login, server juga akan meminta client untuk input id dan passwordnya lalu server akan mencari data di users.txt yang sesuai dengan input client. Jika data yang sesuai ditemukan, maka client dapat login dan dapat menggunakan command-command yang ada pada sistem
- Sistem memiliki sebuah database pada server untuk menampung problem atau soal-soal yang ada pada online judge. Database ini bernama problems.tsv yang terdiri dari judul problem dan author problem (berupa username dari author), yang dipisah dengan \t. File otomatis dibuat saat server dijalankan.
- Client yang telah login, dapat memasukkan command yaitu ‘add’ yang berfungsi untuk menambahkan problem/soal baru pada sistem.
- Client yang telah login, dapat memasukkan command ‘see’ yang berguna untuk menampilkan seluruh judul problem yang ada beserta authornya(author merupakan username client yang menambahkan problem tersebut).
- Client yang telah login, dapat memasukkan command ‘download <judul-problem>’ yang berguna untuk mendownload file description.txt dan input.txt yang berada pada folder pada server dengan nama yang sesuai dengan argumen kedua pada command yaitu <judul-problem>. Kedua file tersebut akan disimpan ke folder dengan nama <judul-problem> di client.
- Client yang telah login, dapat memasukkan command ‘submit <judul-problem> <path-file-output.txt>’.  Command ini berguna untuk melakukan submit jawaban dari client terhadap problem tertentu. Algoritma yang dijalankan adalah client akan mengirimkan file output.txt nya melalui argumen ke 3 pada command, lalu server akan menerima dan membandingkan isi file output.txt yang telah dikirimkan oleh client dan output.txt yang ada pada folder dengan nama yang sesuai dengan argumen ke 2 pada command. Jika file yang dibandingkan sama, maka server akan mengirimkan pesan “AC” dan jika tidak maka server akan mengeluarkan pesan “WA”.
- Server dapat menangani multiple-connection. Dimana jika terdapat 2 atau lebih client yang terhubung ke server, maka harus menunggu sampai client pertama keluar untuk bisa melakukan login dan mengakses aplikasinya.

## Penyelesaian
> Kami belum bisa menyelesaikan nomor ini karena belum menemukan solusi untuk menjawabnya.

# Soal 3

## Persoalan

- Hal pertama yang perlu dilakukan oleh Nami adalah mengextract zip yang diberikan ke dalam folder “/home/[user]/shift3/”. Kemudian working directory program akan berada pada folder “/home/[user]/shift3/hartakarun/”. Karena Nami tidak ingin ada file yang tertinggal, program harus mengkategorikan seluruh file pada working directory secara rekursif
- Semua file harus berada di dalam folder, jika terdapat file yang tidak memiliki ekstensi, file disimpan dalam folder “Unknown”. Jika file hidden, masuk folder “Hidden”.
- Agar proses kategori bisa berjalan lebih cepat, setiap 1 file yang dikategorikan dioperasikan oleh 1 thread
- Untuk mengirimkan file ke Cocoyasi Village, nami menggunakan program client-server. Saat program client dijalankan, maka folder /home/[user]/shift3/hartakarun/” akan di-zip terlebih dahulu dengan nama
  “hartakarun.zip” ke working directory dari program client.
- Client dapat mengirimkan file “hartakarun.zip” ke server dengan mengirimkan
  command berikut ke server

## Penyelesaian

> File di unzip secara manual kedalam folder “/home/[user]/shift3/

```c++
void make_dir(char filename[])
{

        //Get File Name
        char name[100];
        strcpy(name, filename);

        //Get Extension
        ext = strchr(name,'.');


        //Creating Folder Name
        char temp[100];
        strcpy(temp, "./hartakarun/");
        strcat(temp, ext);
        printf("\n%s", temp);

        //Create folder
        status = mkdir(temp, S_IRWXU);
}
```

> Membuat function yang didalamnya terdapat fungsi untuk mendapatkan nama file dan jenis extensionnya.kemudian membuat folder baru sesuai dengan jenis extensionnya.

```c++
int main()
{
    DIR* dir = opendir("./hartakarun");
    if(dir == NULL){
        return 1;
    }

    struct dirent* entity;
    entity = readdir(dir);
    while(entity != NULL){
        char temp[100];
        strcpy(temp, entity->d_name);
        make_dir(temp);

        entity = readdir(dir);
    }

    closedir(dir);

   return 0;
}
```

> Didalam fungsi main dibuat terlebih dahulu fungsi untuk membuka folder jika folder tidak ada akan di return 1.Kemudian akan dipanggil fungsi make_dir.Terakhir akan dilakukan penutupan direktori yang sudah dibuka sebelumnya
