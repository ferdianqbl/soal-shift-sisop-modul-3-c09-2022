#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

pthread_t unzipId[2];

void *downloadFile(char *down_arg[])
{
  pid_t id = fork();
  int status;
  if (id == 0)
  {
    execv("/bin/wget", down_arg);
  }
  else
  {
    while ((wait(&status)) > 0)
      ;
    printf("download file success\n");
  }
}
void *unzipFile(char *unzip_arg[])
{
  pid_t id = fork();
  int status;
  if (id == 0)
  {
    execv("/bin/unzip", unzip_arg);
  }
  else
  {
    while ((wait(&status)) > 0)
      ;
    printf("unzip file success\n");
  }
}
void rmFile(char *rm_arg[])
{
  pid_t id = fork();
  int status;
  if (id == 0)
  {
    execv("/bin/rm", rm_arg);
  }
  else
  {
    while ((wait(&status)) > 0)
      ;
    printf("remove file success\n");
  }
}
void base64(char *base_arg[])
{
  pid_t id = fork();
  int status;
  if (id == 0)
  {
    execv("/bin/base64", base_arg);
  }
  else
  {
    while ((wait(&status)) > 0)
      ;
    printf("base64 success\n");
  }
}
// thread Function
void *unzip(void *arg)
{
  char *unz_args[] = {"unzip", "-q", "music.zip", "-d", "music", NULL};
  char *unz_args2[] = {"unzip", "-q", "quote.zip", "-d", "quote", NULL};

  pthread_t id = pthread_self();
  if (pthread_equal(id, unzipId[0]))
  {
    unzipFile(unz_args);
  }
  else if (pthread_equal(id, unzipId[1]))
  {
    unzipFile(unz_args2);
  }
  return NULL;
}

int main()
{
  // download file
  char *down_args[] = {"wget", "-q", "https://docs.google.com/uc?export=download&id=1_djk0z-cx8bgISFsMiUaXzty2cT4VZp1", "-O", "music.zip", NULL};
  char *down_args2[] = {"wget", "-q", "https://docs.google.com/uc?export=download&id=1jR67_JAyozZPs2oYtYqEZxeBLrs-k3dt", "-O", "quote.zip", NULL};
  downloadFile(down_args);
  downloadFile(down_args2);

  // unzip file
  pthread_create(&unzipId[0], NULL, &unzip, NULL);
  pthread_create(&unzipId[1], NULL, &unzip, NULL);
  pthread_join(unzipId[0], NULL);
  pthread_join(unzipId[1], NULL);

  // remove zip
  char *rm_args[] = {"rm", "music.zip", NULL};
  char *rm_args2[] = {"rm", "quote.zip", NULL};
  rmFile(rm_args);
  rmFile(rm_args2);

  // base64
  char *base_args[] = {"base64", "-d", "music/m1.txt", NULL};
  base64(base_args);
}